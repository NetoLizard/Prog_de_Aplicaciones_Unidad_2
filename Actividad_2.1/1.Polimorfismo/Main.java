/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polimorfismo;

/**
 *
 * by Ernesto Lerma Carrasco
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Clase_B[] bs = new Clase_B[3];
        bs[0] = new Clase_B();
        bs[1] = new Clase_A();
        bs[2] = new Clase_C();
        
        naivePrinter(bs);
    }
    
    private static void naivePrinter(Clase_B[] bs) {
        for (int i = 0; i < bs.length; i++) {
            bs[i].print();
        }
    }
}
