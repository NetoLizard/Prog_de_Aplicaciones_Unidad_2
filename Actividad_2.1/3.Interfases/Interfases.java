/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfases;

/**
 *
 * @author Ernesto Lerma Carrasco
 */
public class Interfases {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Carro c = new Carro();
        Bicicleta b = new Bicicleta();
        c.avanzar();
        b.avanzar();
        b.sentarse();
    }
    
}
