/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing_jlabel;

/**
 *
 * @author Ernesto Lerma Carrasco
 */
import javax.swing.*;
public class Swing_JLabel extends JFrame{
    private JLabel label1,label2;
    public Swing_JLabel() {
        setLayout(null);
        label1=new JLabel("Sistema de Facturación.");
        label1.setBounds(10,20,300,30);
        add(label1);
        label2=new JLabel("Vesion 1.0");
        label2.setBounds(10,100,100,30);
        add(label2);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Swing_JLabel JLabel1 = new Swing_JLabel();
        JLabel1.setBounds(0,0,300,200);
        JLabel1.setResizable(false);
        JLabel1.setVisible(true);
    }
    
}
