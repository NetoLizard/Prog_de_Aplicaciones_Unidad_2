/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ernesto Lerma Carrasco
 */
import javax.swing.*;
public class Formulario1 extends JFrame{
    private JTextField textfield1;
    private JScrollPane scrollpane1;
    private JTextArea textarea1;
    public Formulario1(){
        setLayout(null);
        textfield1=new JTextField();
        textfield1.setBounds(10,10,200,30);
        add(textfield1);
        textarea1=new JTextArea();        
        scrollpane1=new JScrollPane(textarea1);    
        scrollpane1.setBounds(10,50,400,300);
        add(scrollpane1);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Formulario1 formulariox=new Formulario1();
        formulariox.setBounds(0,0,540,400);
        formulariox.setVisible(true);
    }
    
}
