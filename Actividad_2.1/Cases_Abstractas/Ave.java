/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases_Abstractas;

/**
 *
 * @author Ernesto Lerma Carrasco
 */
public class Ave extends Animal {
    public Ave() {
    super();
    setNombre("ave");
    }
    public void moverse() {
        System.out.println("El ave está volando");
    }
    
}
